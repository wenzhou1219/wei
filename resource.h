//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Wei.rc
//
#define IDD_WEI_DIALOG                  102
#define IDR_MAINFRAME                   128
#define IDE_SPEED1                      1000
#define IDE_SPEED2                      1001
#define IDB_HOME                        1002
#define IDB_MOVE                        1003
#define IDB_STOP                        1004
#define IDB_CIRCLEMOVE                  1005
#define IDB_BACK                        1006
#define IDE_CIRCLE_STEP                 1007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
