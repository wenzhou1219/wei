// WeiDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Wei.h"
#include "WeiDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWeiDlg dialog

CWeiDlg::CWeiDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWeiDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWeiDlg)
	p1=50;
	p2=100;
	p3 = 1000;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CWeiDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWeiDlg)
	DDX_Text(pDX, IDE_SPEED1, p1);
	DDV_MinMaxInt(pDX, p1, 0, 500);
	DDX_Text(pDX, IDE_SPEED2, p2);
	DDV_MinMaxInt(pDX, p2, 0, 500);
	DDX_Text(pDX, IDE_CIRCLE_STEP, p3);
	DDV_MinMaxInt(pDX, p3, 0, 2000);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CWeiDlg, CDialog)
	//{{AFX_MSG_MAP(CWeiDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDB_HOME, OnHome)
	ON_BN_CLICKED(IDB_MOVE, OnMove)
	ON_BN_CLICKED(IDB_STOP, OnStop)
	ON_BN_CLICKED(IDB_BACK, OnBack)
	ON_BN_CLICKED(IDB_CIRCLEMOVE, OnCirclemove)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWeiDlg message handlers

BOOL CWeiDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CWeiDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CWeiDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CWeiDlg::OnHome() 
{
	// TODO: Add your control notification handler code here
	TCHAR szBuffer[256];

	PmacGetResponse(dwDeviceNum, szBuffer, 256, "#7j/#8j/") ;	//激活电机
	PmacGetResponse(dwDeviceNum, szBuffer, 256, "&1B20R") ;		//回零
}

void CWeiDlg::OnMove() 
{
	// TODO: Add your control notification handler code here
	//界面参数传递到p变量中
	if (UpdateData(TRUE))
	{
		//p变量传递到PMAC中
		PmacSetPInt("p1", p1);
		PmacSetPInt("p2", p2);
		
		//调用运动程序
		TCHAR szBuffer[256];
		PmacGetResponse(dwDeviceNum, szBuffer, 256, "&1B30R") ;
	}	
}

void CWeiDlg::OnCirclemove() 
{
	// TODO: Add your control notification handler code here
	//界面参数传递到p变量中
	if (UpdateData(TRUE))
	{
		//p变量传递到PMAC中
		PmacSetPInt("p1", p1);
		PmacSetPInt("p2", p2);
		PmacSetPInt("p3", p3);
		
		//调用运动程序
		TCHAR szBuffer[256];
		PmacGetResponse(dwDeviceNum, szBuffer, 256, "&1B50R") ;
	}	
}


void CWeiDlg::OnBack() 
{
	// TODO: Add your control notification handler code here
	//调用回到初始位置程序
	TCHAR szBuffer[256];
	PmacGetResponse(dwDeviceNum, szBuffer, 256, "&1B40R") ;
}

void CWeiDlg::OnStop() 
{
	// TODO: Add your control notification handler code here
	TCHAR szBuffer[256];

	PmacGetResponse(dwDeviceNum, szBuffer, 256, "#7k#8k") ;//关闭电机
}

void CWeiDlg::PmacSetPInt(char *pStr, int p)
{
	TCHAR szBuffer[256] ;
	TCHAR szTemp[256];
	sprintf(szBuffer, "%s=%d", pStr, p) ;
	
	PmacGetResponse(dwDeviceNum, szTemp, 256, szBuffer);
}
