; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CWeiDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Wei.h"

ClassCount=2
Class1=CWeiApp
Class2=CWeiDlg

ResourceCount=3
Resource2=IDR_MAINFRAME
Resource3=IDD_WEI_DIALOG

[CLS:CWeiApp]
Type=0
HeaderFile=Wei.h
ImplementationFile=Wei.cpp
Filter=N

[CLS:CWeiDlg]
Type=0
HeaderFile=WeiDlg.h
ImplementationFile=WeiDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDE_CIRCLE_STEP



[DLG:IDD_WEI_DIALOG]
Type=1
Class=CWeiDlg
ControlCount=12
Control1=IDCANCEL,button,1342242816
Control2=IDC_STATIC,static,1342308352
Control3=IDE_SPEED1,edit,1350631552
Control4=IDC_STATIC,static,1342308352
Control5=IDE_SPEED2,edit,1350631552
Control6=IDB_HOME,button,1342242816
Control7=IDB_MOVE,button,1342242816
Control8=IDB_STOP,button,1342242816
Control9=IDB_BACK,button,1342242816
Control10=IDB_CIRCLEMOVE,button,1342242816
Control11=IDE_CIRCLE_STEP,edit,1350631552
Control12=IDC_STATIC,static,1342308352

