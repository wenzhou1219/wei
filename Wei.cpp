// Wei.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Wei.h"
#include "WeiDlg.h"
#include "pmacruntime.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWeiApp

BEGIN_MESSAGE_MAP(CWeiApp, CWinApp)
	//{{AFX_MSG_MAP(CWeiApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWeiApp construction

CWeiApp::CWeiApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CWeiApp object

CWeiApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CWeiApp initialization

BOOL CWeiApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	if(NULL == PmacRuntimeLink())//����PMAC��̬���ӿ⣬����PMAC
	{
		MessageBox(NULL, TEXT("����PMAC��ʧ��"), TEXT("����"), MB_OK | MB_ICONSTOP) ;
	}
	else
	{
		//��PMAC down������
		PmacDownload(dwDeviceNum, NULL, NULL, NULL, ".\\home.pmc", FALSE, FALSE, FALSE, TRUE);
 		PmacDownload(dwDeviceNum, NULL, NULL, NULL, ".\\move.pmc", FALSE, FALSE, FALSE, TRUE);
		PmacDownload(dwDeviceNum, NULL, NULL, NULL, ".\\back.pmc", FALSE, FALSE, FALSE, TRUE);
		PmacDownload(dwDeviceNum, NULL, NULL, NULL, ".\\circlemove.pmc", FALSE, FALSE, FALSE, TRUE);

		CWeiDlg dlg;
		m_pMainWnd = &dlg;
		int nResponse = dlg.DoModal();
		if (nResponse == IDOK)
		{
			// TODO: Place code here to handle when the dialog is
			//  dismissed with OK
		}
		else if (nResponse == IDCANCEL)
		{
			// TODO: Place code here to handle when the dialog is
			//  dismissed with Cancel
		}
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	CloseRuntimeLink();//ж��PMAC��̬���ӿ�
	return FALSE;
}
